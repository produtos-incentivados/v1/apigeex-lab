import json

with open("api-webhook-bvs/config.json", encoding='utf-8') as projectInfo:
    dados = json.load(projectInfo)

projectName = dados.get("project")
print(projectName)

with open('script/.env', 'w') as writer:
     writer.write(f'export PROJECTNAME="{projectName}"')